"""projet_lp_web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from urllib import request

from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required

from django.urls import path, include

from app.views.image.create import ImageCreateView
from app.views.image.delete import DeleteImage
from app.views.image.detail import DetailImage
from app.views.image.list import ListImage
from app.views.image.update import UpdateImage
from app.views.index import IndexView
from app.views.person.detail import DetailUser
from app.views.person.create import PersonCreateView
from app.views.person.update import UpdatePerson
from app.views.person.delete import DeletePerson
from projet_lp_web import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', IndexView.as_view(), name='app_index'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('register', PersonCreateView.signup, name='app_register'),
    path('postImage', login_required(ImageCreateView.upload_file), name='app_postImage'),
    path('image/list/<int:pk>', login_required(ListImage.as_view()), name='app_image_list'),
    path('image/detail/<int:pk>', DetailImage.as_view(), name='app_detail_image'),
    path('image/delete/<int:pk>', login_required(DeleteImage.as_view()), name='app_delete_image'),
    path('image/update/<int:pk>', login_required(UpdateImage.as_view()), name='app_update_image'),
    path('profile/<int:pk>', login_required(DetailUser.as_view()), name='app_profile'),
    path('profile/update/<int:pk>', login_required(UpdatePerson.as_view()), name='app_profile_update'),
    path('profile/delete/<int:pk>', login_required(DeletePerson.as_view()), name='app_profile_delete'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
