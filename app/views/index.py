from django.views.generic import ListView
from app.models.image import Image


class IndexView(ListView):
    template_name = 'index.html'

    def get_queryset(self):
        return Image.objects.all()

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        result['images'] = Image.objects.all().order_by('-date_creation')
        return result
