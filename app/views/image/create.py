from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic import CreateView

from app.forms.image import ImageForm
from app.models.image import Image


class ImageCreateView(CreateView):
    template_name = 'image/create.html'
    model = Image
    form_class = ImageForm
    success_url = reverse_lazy('app_myImages')

    def upload_file(request):
        if request.method == 'POST':
            form = ImageForm(request.POST, request.FILES)
            if form.is_valid():
                instance = form.save(commit=False)
                instance.author = request.user
                instance.save()
                return redirect('app_index')
        else:
            form = ImageForm
        return render(request, 'image/create.html', {'form': form})

