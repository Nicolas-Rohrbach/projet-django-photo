from django.http import Http404
from django.urls import reverse_lazy
from django.views.generic import DeleteView

from app.models.image import Image


class DeleteImage(DeleteView):
    template_name = 'image/delete.html'
    model = Image
    success_message = "Deleted Successfully"
    success_url = reverse_lazy('app_index')

    def get_queryset(self):
        qs = super(DeleteImage, self).get_queryset()
        return qs.filter(pk=self.kwargs.get('pk'))
