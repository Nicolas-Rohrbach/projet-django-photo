from django.urls import reverse_lazy
from django.views.generic import UpdateView

from app.models.image import Image


class UpdateImage(UpdateView):
    template_name = 'image/update.html'
    model = Image
    fields = ['title']
    success_url = reverse_lazy('app_index')
