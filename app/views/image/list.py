from django.contrib.auth.models import User
from django.http import request
from django.views.generic import ListView

from app.models.image import Image


class ListImage(ListView):
    template_name = 'image/list.html'
    model = Image
    slug_field = 'pk'

    def get_queryset(self):
        result = Image.objects.filter(author_id=self.kwargs.get('pk'))
        return result
