from django.views.generic import DetailView

from app.models.image import Image


class DetailImage(DetailView):
    template_name = 'image/detail.html'
    model = Image
    slug_field = 'pk'

    def get_context_data(self, **kwargs):
        context = super(DetailImage, self).get_context_data(**kwargs)
        context['image'] = Image.objects.filter(pk=self.kwargs.get('pk'))
        return context
