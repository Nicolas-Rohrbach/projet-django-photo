from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views.generic import DeleteView


class DeletePerson(DeleteView):
    template_name = 'user/delete.html'
    model = User
    success_message = "Deleted Successfully"
    success_url = reverse_lazy('app_index')

    def get_queryset(self):
        qs = super(DeletePerson, self).get_queryset()
        return qs.filter(pk=self.kwargs.get('pk'))
