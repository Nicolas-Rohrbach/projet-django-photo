from django.contrib.auth.models import User
from django.views.generic import DetailView

from app.models.image import Image
from app.models.person import Person


class DetailUser(DetailView):
    template_name = 'user/profile.html'
    model = User
    slug_field = 'pk'

    def get_context_data(self, **kwargs):
        context = super(DetailUser, self).get_context_data(**kwargs)
        context['person'] = Person.objects.filter(user_id=self.kwargs.get('pk'))
        context['images'] = Image.objects.filter(author_id=self.kwargs.get('pk'))
        return context
