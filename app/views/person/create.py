from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from django.views.generic import CreateView
from app.forms.person import PersonForm


class PersonCreateView(CreateView):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.POST = None
        self.method = None

    def signup(request):
        if request.method == 'POST':
            form = PersonForm(request.POST)
            if form.is_valid():
                user = form.save()
                user.refresh_from_db()  # load the profile instance created by the signal
                user.person.bio = form.cleaned_data.get('bio')
                user.save()
                raw_password = form.cleaned_data.get('password1')
                user = authenticate(username=user.username, password=raw_password)
                login(request, user)
                return redirect('app_index')
        else:
            form = PersonForm()
        return render(request, 'user/register.html', {'form': form})
