from django.contrib.auth import logout
from django.shortcuts import redirect


def logout_view(request):
    try:
        del request.session['user_id']
    except KeyError:
        pass
    logout(request)
    redirect('app_index')
