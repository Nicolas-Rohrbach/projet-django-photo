from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.views.generic import UpdateView
from app.forms.updatePerson import UpdatePersonForm
from app.models.person import Person


class UpdatePerson(UpdateView):
    form_class = UpdatePersonForm
    model = User
    template_name = 'user/update.html'

    def get(self, request, **kwargs):
        self.object = User.objects.get(username=self.request.user)
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        context = self.get_context_data(object=self.object, form=form)
        return self.render_to_response(context)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        person = Person.objects.get(user=self.request.user)
        person.avatar = form.cleaned_data['avatar']
        person.save()
        self.object.user = self.request.user
        self.object.save()
        return redirect('app_index')

    def get_object(self, queryset=None):
        return self.request.user

