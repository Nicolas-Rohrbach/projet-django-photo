from django.conf import settings
from django.db import models


class Image(models.Model):
    title = models.CharField(max_length=60)
    photo = models.FileField(upload_to='publication')
    date_creation = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        result = self.title
        return result
