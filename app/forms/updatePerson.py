from django import forms
from django.contrib.auth.models import User


class UpdatePersonForm(forms.ModelForm):
    avatar = forms.FileField(required=None)

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password']
