from django import forms
from app.models.image import Image


class ImageForm(forms.ModelForm):

    class Meta:
        model = Image
        fields = ['title', 'photo']
