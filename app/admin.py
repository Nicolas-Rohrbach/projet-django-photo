from django.contrib import admin

# Register your models here.
from app.models.image import Image
from app.models.person import Person

admin.site.register(Person)
admin.site.register(Image)

